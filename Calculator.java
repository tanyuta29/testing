import com.google.inject.Inject;

import java.util.Scanner;

public class Calculator {
    @Inject
    public static Scanner scanner;

    public static int getInt() {
        System.out.println("Enter number");
        int num;
        if (scanner.hasNextInt()) {
            num = scanner.nextInt();
        } else {
            System.out.println("You made error, try agan");
            scanner.next();
            num = getInt();
        }
        return num;
    }

    public static int getInt(int i) {
        return i;
    }
}

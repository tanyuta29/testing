import com.google.inject.Inject;

import java.util.Scanner;

public class Operation {
    @Inject
   public static Scanner scanner;
    public static char getOperation(){
        System.out.println("Enter operation");
        char operation;
        if (scanner.hasNext()){
            operation = scanner.next().charAt(0);
        }else {
            System.out.println("You made error, the agan ");
            scanner.next();
            operation=getOperation();
        }
        return operation;
    }
}

import com.google.inject.Guice;
import com.google.inject.Inject;

import java.util.Scanner;

public class Calculate {
    @Inject
    private Operation operation;
    @Inject
    private Calculate calculate;
    @Inject
    private Scanner scanner;
    @Inject
    private SwitchSystem switchSystem;

    public static void main(String[] args) {
        Guice.createInjector(new MainModule());
        int num1 = Calculator.getInt();
        int num2 = Calculator.getInt();
        char operation = Operation.getOperation();
        int result = SwitchSystem.calc(num1, num2, operation);
        System.out.println("Answer " + result);
    }
}
